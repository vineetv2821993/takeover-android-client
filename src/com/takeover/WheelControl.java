package com.takeover;


import java.io.IOException;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.takeover.MjpegActivity.DoRead;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;


public class WheelControl extends Activity implements SensorEventListener{
	//
    private static final String TAG = "WheelControl";

    private MjpegView mv;
	//
	protected static final String SWIPE_SCREEN = "100";

	private boolean mInitialized;

	private SensorManager mSensorManager;

	private Sensor mAccelerometer;

	private final float NOISE = (float) 0.5;
	private final String CMD_MOUSE = "c:\\command\\";

	private final float alpha = (float) 0.8;
//	private float gravity[] = new float[3];
	private float x = (float) 0.0;
	private float y = (float) 0.0;
	private float z = (float) 0.0;
	private int jCommand = 0;
	private final float X_NOISE = (float) 3.0;
	private final float Y_NOISE = (float) 3.0;
	private final int UP = 65;
	private final int DOWN = -67;
	private final int LEFT = 66;
	private final int RIGHT = 68;

    @SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_touch_pad);
        
        String URL = "http://"+IPInfo.getServerIP()+":8081/";
        //String URL = " ";
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
        WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mv = new MjpegView(this);
        setContentView(mv);        
        new DoRead().execute(URL);

        
//		FrameLayout setLayout = (FrameLayout) findViewById(R.id.touchframe);
//		TextView setLayout = (TextView) findViewById(R.id.fullscreen_content);
//		setLayout.setOnTouchListener(new View.OnTouchListener() {
//			
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//				// TODO Auto-generated method stub
//				if(event.getAction()==MotionEvent.ACTION_MOVE){
//					ClientThread.setCommand(SWIPE_SCREEN);
//					ClientThread.myThread = new Thread(new ClientThread());
//					ClientThread.myThread.start();
//				}
//				return false;
//			}
//		});

        mInitialized = false;
        
        ClientThread.SERVER_IP = IPInfo.getServerIP();

        Toast.makeText(getBaseContext(), "Key Configuration\n\nAccelerate: Tilt Up = Up Key\nBreak: Tilt Down = Down Key\nMove Left: Tilt Left\nMove Right: Tilt Right = Right Key", Toast.LENGTH_LONG).show();

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

    }
    protected void onResume() {

    	super.onResume();

    	mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

    	}
//
    
    public class DoRead extends AsyncTask<String, Void, MjpegInputStream> {
        protected MjpegInputStream doInBackground(String... url) {
            //TODO: if camera has authentication deal with it and don't just not work
            HttpResponse res = null;
            DefaultHttpClient httpclient = new DefaultHttpClient();     
            Log.d(TAG, "1. Sending http request");
            try {
                res = httpclient.execute(new HttpGet(URI.create(url[0])));
                Log.d(TAG, "2. Request finished, status = " + res.getStatusLine().getStatusCode());
                if(res.getStatusLine().getStatusCode()==401){
                    //You must turn off camera User Access Control before this will work
                    return null;
                }
                return new MjpegInputStream(res.getEntity().getContent());  
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                Log.d(TAG, "Request failed-ClientProtocolException", e);
                //Error connecting to camera
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "Request failed-IOException", e);
                //Error connecting to camera
            }

            return null;
        }

        protected void onPostExecute(MjpegInputStream result) {
            mv.setSource(result);
            mv.setDisplayMode(MjpegView.SIZE_BEST_FIT);
            mv.showFps(true);
        }
    }
    //
    	protected void onPause() {

    	super.onPause();
        mv.stopPlayback();
    	mSensorManager.unregisterListener(this);

    	}

    	@Override

    	public void onAccuracyChanged(Sensor sensor, int accuracy) {

    	// can be safely ignored for this demo

    	}
    	
    	@Override

    	public void onSensorChanged(SensorEvent event) {

//    	TextView tvX= (TextView)findViewById(R.id.x_axis);
//
//    	TextView tvY= (TextView)findViewById(R.id.y_axis);
//
//    	TextView tvZ= (TextView)findViewById(R.id.z_axis);

    	//ImageView iv = (ImageView)findViewById(R.id.image);

    	x = event.values[0];

    	y = event.values[1];

    	z = event.values[2];

    	if (!mInitialized) {
//
//    	tvX.setText("0.0");
//
//    	tvY.setText("0.0");
//
//    	tvZ.setText("0.0");

    	mInitialized = true;

    	} else {

    		  // In this example, alpha is calculated as t / (t + dT),
    		  // where t is the low-pass filter's time-constant and
    		  // dT is the event delivery rate.


    		  // Isolate the force of gravity with the low-pass filter.
//    		  gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
//    		  gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
//    		  gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];

    		  // Remove the gravity contribution with the high-pass filter.
    		  x = event.values[0];// - gravity[0];
    		  y = event.values[1];// - gravity[1];
    		  z = event.values[2];// - gravity[2];
    		  x = (float) ((Math.abs(x)>NOISE)?x:0.0);
    		  y = (float) ((Math.abs(y)>NOISE)?y:0.0);
    		  z = (float) ((Math.abs(z)>NOISE)?z:0.0);
//    	tvX.setText(Float.toString(x));
//
//    	tvY.setText(Float.toString(y));
//
//    	tvZ.setText(Float.toString(z));
    	//Executing Command
//    	if(x>2.0){
//    		ClientThread.setCommand("38");
//    		ClientThread.myThread = new Thread(new ClientThread());
//    		ClientThread.myThread.start();    		    		
//    		
////    		ClientThread.setCommand(CMD_MOUSE+"nircmd sendkey down press");
////    		ClientThread.myThread = new Thread(new ClientThread());
////    		ClientThread.myThread.start(); 
//    	}
//    	else if(x<-2.0){
//    		ClientThread.setCommand("40");
//    		ClientThread.myThread = new Thread(new ClientThread());
//    		ClientThread.myThread.start();    		
////    		ClientThread.setCommand(CMD_MOUSE+"nircmd sendkey up press");
////    		ClientThread.myThread = new Thread(new ClientThread());
////    		ClientThread.myThread.start(); 
//    	}
////    	else{
////    		ClientThread.setCommand(CMD_MOUSE+"nircmd sendkey down up");
////    		ClientThread.myThread = new Thread(new ClientThread());
////    		ClientThread.myThread.start();    		
////    		ClientThread.setCommand(CMD_MOUSE+"nircmd sendkey up up");
////    		ClientThread.myThread = new Thread(new ClientThread());
////    		ClientThread.myThread.start();     		
////    	}
//    	if(y>3.0){
//    		ClientThread.setCommand("37");
//    		ClientThread.myThread = new Thread(new ClientThread());
//    		ClientThread.myThread.start();    		
////    		ClientThread.setCommand(CMD_MOUSE+"nircmd sendkey right up");
////    		ClientThread.myThread = new Thread(new ClientThread());
////    		ClientThread.myThread.start(); 
//    	}
//    	else if(y<-3.0){
//    		ClientThread.setCommand("39");
//    		ClientThread.myThread = new Thread(new ClientThread());
//    		ClientThread.myThread.start();    		
////    		ClientThread.setCommand(CMD_MOUSE+"nircmd sendkey left up");
////    		ClientThread.myThread = new Thread(new ClientThread());
////    		ClientThread.myThread.start(); 
//    	}
    	jCommand = 0;
    	if(x>X_NOISE)
    		jCommand = UP;
    	else if(x<-X_NOISE)
    		jCommand = DOWN;
    	if(y>Y_NOISE)
    		jCommand+= RIGHT;
    	else if(y<-Y_NOISE)
    		jCommand+= LEFT;
    	if(jCommand!=0){
    		ClientThread.setCommand(String.valueOf(jCommand));
    		ClientThread.myThread = new Thread(new ClientThread());
    		ClientThread.myThread.start(); 
    	}
//    	else{
//    		ClientThread.setCommand(CMD_MOUSE+"nircmd sendkey right up");
//    		ClientThread.myThread = new Thread(new ClientThread());
//    		ClientThread.myThread.start();    		
//    		ClientThread.setCommand(CMD_MOUSE+"nircmd sendkey left up");
//    		ClientThread.myThread = new Thread(new ClientThread());
//    		ClientThread.myThread.start();     		
//    	}

//    	iv.setVisibility(View.VISIBLE);
//
////    	if (deltaX > deltaY) {
////
////    	iv.setImageResource(R.drawable.horizontal);
////
////    	} else if (deltaY > deltaX) {
////
////    	iv.setImageResource(R.drawable.vertical);
////
////    	} else {
//
//    	iv.setVisibility(View.INVISIBLE);

//    	}

    	}

    	}

}
