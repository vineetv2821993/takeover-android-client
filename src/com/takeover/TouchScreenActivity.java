package com.takeover;



import java.io.IOException;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

public class TouchScreenActivity extends Activity {
    private static final String TAG = "MjpegActivity";
	private final String CMD_PROMPT = "c:\\command\\";
	private int x,y, x_curr, y_curr;;
	static boolean state = true;
	
	static int width ,height, Dwidth, Dheight;

    private MjpegView mv;

    @SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		width = size.x;
		height = size.y;
		Dwidth = 1366; 
		Dheight = 768;
        //sample public cam
     //   String URL = "http://trackfield.webcam.oregonstate.edu/axis-cgi/mjpg/video.cgi?resolution=800x600&amp%3bdummy=1333689998337";
        String URL = "http://"+IPInfo.getServerIP()+":12120/";
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
        WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mv = new MjpegView(this);
        
        setContentView(mv);        
mv.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub

				double X = Dwidth*event.getRawX()/width;
				if(((int)(X*100))%100<=50)
					x_curr = (int) Math.floor(X);
				else
					x_curr = (int) Math.ceil(X);
				double Y = Dheight*event.getRawY()/height;
				if(((int)(Y*100))%100<=50)
					y_curr = (int) Math.floor(Y);
				else
					y_curr = (int) Math.ceil(Y);
				

						ClientThread.setCommand(CMD_PROMPT+"nircmd setcursor "+x_curr+" "+y_curr);
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();
//						ClientThread.setCommand(CMD_PROMPT+"nircmd sendmouse left click");
//						ClientThread.myThread = new Thread(new ClientThread());
//						ClientThread.myThread.start();


				return false;
			}
		});

        new DoRead().execute(URL);
    }

    public void onPause() {
        super.onPause();
        mv.stopPlayback();
    }

    public class DoRead extends AsyncTask<String, Void, MjpegInputStream> {
        protected MjpegInputStream doInBackground(String... url) {
            //TODO: if camera has authentication deal with it and don't just not work
            HttpResponse res = null;
            DefaultHttpClient httpclient = new DefaultHttpClient();     
            Log.d(TAG, "1. Sending http request");
            try {
                res = httpclient.execute(new HttpGet(URI.create(url[0])));
                Log.d(TAG, "2. Request finished, status = " + res.getStatusLine().getStatusCode());
                if(res.getStatusLine().getStatusCode()==401){
                    //You must turn off camera User Access Control before this will work
                    return null;
                }
                return new MjpegInputStream(res.getEntity().getContent());  
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                Log.d(TAG, "Request failed-ClientProtocolException", e);
                //Error connecting to camera
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "Request failed-IOException", e);
                //Error connecting to camera
            }

            return null;
        }

        protected void onPostExecute(MjpegInputStream result) {
            mv.setSource(result);
            mv.setDisplayMode(MjpegView.SIZE_BEST_FIT);
            mv.showFps(true);
        }
    }
}

