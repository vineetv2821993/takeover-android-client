package com.takeover;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class IPInfo extends Activity {

	protected static final int RESULT_CLOSE_ALL = 2020;
	protected static final String CLIENT_ACTIVITY = "com.takeover.Client";
	//private static Button setServerIP;
	private static ImageView setServerIPIM;
	private static EditText serverIP;
	private static String serverIPAdd;
	public static String getServerIP(){
		return serverIPAdd;
	}
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ipinfo);
		//
		HttpThread.myThread = new Thread(new HttpThread());
		HttpThread.myThread.start();		
		 //
		//setServerIP = (Button) findViewById(R.id.setServerIP);
		setServerIPIM = (ImageView) findViewById(R.id.imageSetIP);
		serverIP = (EditText) findViewById(R.id.serverIP);
		setServerIPIM.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				serverIPAdd = serverIP.getText().toString();

				Intent NextActivity = new Intent(CLIENT_ACTIVITY);
				startActivity(NextActivity);
//				ClientThread.setCommand("xcopy command C:\\ /s/t");
//				ClientThread.myThread = new Thread(new ClientThread());
//				ClientThread.myThread.start();
			}
		});
	}


}
