package com.takeover;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

public class NavigationPanel extends Activity {

	final String UP = "201";
	final String DOWN = "202";
	final String LEFT = "203";
	final String RIGHT = "204";
	final String ENTER = "205";
	final String ESC = "206";
	final String BACKSPACE = "207";
	final String HOME = "208";
	final String END = "209";
	final String  TAB= "210";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		   requestWindowFeature(Window.FEATURE_NO_TITLE);
	        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
	        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_navigation_panel);
		
		ImageButton up = (ImageButton) findViewById(R.id.imageButtonUp);
		up.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		    ClientThread.setCommand(UP);
			ClientThread.myThread = new Thread(new ClientThread());
			ClientThread.myThread.start();
				
			}
		});
		
		ImageButton left = (ImageButton) findViewById(R.id.imageButtonLeft);
		left.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		    ClientThread.setCommand(LEFT);
			ClientThread.myThread = new Thread(new ClientThread());
			ClientThread.myThread.start();
				
			}
		});
		
		ImageButton right = (ImageButton) findViewById(R.id.imageButtonRight);
		right.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		    ClientThread.setCommand(RIGHT);
			ClientThread.myThread = new Thread(new ClientThread());
			ClientThread.myThread.start();
				
			}
		});
		
		ImageButton down = (ImageButton) findViewById(R.id.imageButtonDown);
		down.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		    ClientThread.setCommand(DOWN);
			ClientThread.myThread = new Thread(new ClientThread());
			ClientThread.myThread.start();
				
			}
		});
		
		ImageButton enter = (ImageButton) findViewById(R.id.imageButtonEnter);
		enter.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		    ClientThread.setCommand(ENTER);
			ClientThread.myThread = new Thread(new ClientThread());
			ClientThread.myThread.start();
				
			}
		});
		
		ImageButton bckspc = (ImageButton) findViewById(R.id.imageButtonBckspc);
		bckspc.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		    ClientThread.setCommand(BACKSPACE);
			ClientThread.myThread = new Thread(new ClientThread());
			ClientThread.myThread.start();
				
			}
		});
		
		ImageButton esc = (ImageButton) findViewById(R.id.imageButtonEsc);
		esc.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		    ClientThread.setCommand(ESC);
			ClientThread.myThread = new Thread(new ClientThread());
			ClientThread.myThread.start();
				
			}
		});
		
		ImageButton tab = (ImageButton) findViewById(R.id.imageButtonTab);
		tab.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		    ClientThread.setCommand(TAB);
			ClientThread.myThread = new Thread(new ClientThread());
			ClientThread.myThread.start();
				
			}
		});
		
		ImageButton home = (ImageButton) findViewById(R.id.imageButtonHome);
		home.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		    ClientThread.setCommand(HOME);
			ClientThread.myThread = new Thread(new ClientThread());
			ClientThread.myThread.start();
				
			}
		});
		
		ImageButton end = (ImageButton) findViewById(R.id.imageButtonEnd);
		end.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		    ClientThread.setCommand(END);
			ClientThread.myThread = new Thread(new ClientThread());
			ClientThread.myThread.start();
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.navigation_panel, menu);
		return true;
	}

}
