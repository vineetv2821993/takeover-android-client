package com.takeover;


import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class TerminalActivity extends Activity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.terminal);
		ClientThread.SERVER_IP = IPInfo.getServerIP();
		Toast.makeText(getBaseContext(), ClientThread.SERVER_IP, Toast.LENGTH_LONG).show();
		//Terminal Command
		Button ClickSend = (Button) findViewById(R.id.terminalCmd);
		ClickSend.setOnClickListener(new View.OnClickListener() {
					
		@Override
			public void onClick(View v) {
						// TODO Auto-generated method stub
						EditText et = (EditText) findViewById(R.id.terminalEditText);
						ClientThread.setCommand(et.getText().toString());
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();
						TextView ServerText = (TextView) findViewById(R.id.retunTextView);
						ServerText.setText(ClientThread.ServerText);
			}
		});


		
	}


}
