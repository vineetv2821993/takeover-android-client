package com.takeover;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class WMPActivity extends Activity {

	public final String COMMAND_TOOL = "c:\\command\\nircmd ";
	public static int initProgress = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ClientThread.setCommand(COMMAND_TOOL+"exec max \"C:\\Program Files\\Windows Media Player\\wmplayer.exe\"");
		ClientThread.myThread = new Thread(new ClientThread());
		ClientThread.myThread.start();
		setContentView(R.layout.activity_wmp);
		
		//Master Control

		SeekBar audioSeekBar = (SeekBar) findViewById(R.id.seekBarMASTER);
		audioSeekBar.setMax(65535);
		OnSeekBarChangeListener audioChangeListener = new OnSeekBarChangeListener(){

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				ClientThread.setCommand(COMMAND_TOOL+"setsysvolume "+progress);
				//Toast.makeText(getBaseContext(), Command, Toast.LENGTH_LONG).show();				
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

		};
		audioSeekBar.setOnSeekBarChangeListener(audioChangeListener);
		
		//VLC Control
		

		SeekBar audioSeekBarVLC = (SeekBar) findViewById(R.id.seekBarVLC);
		audioSeekBarVLC.setMax(10);

		OnSeekBarChangeListener audioChangeListenerVLC = new OnSeekBarChangeListener(){

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				int progD = progress - initProgress;
				int loopN = 0;
				String cmd = "";
				if(progress == 0){
					loopN=16;
					cmd="F8";
				}
				else if(progress == 10){
					loopN=10;
					cmd="F9";
				}
				else if(progD>0){
					loopN=progD;
					cmd="F9";
				}
				else if(progD<0){
					loopN=-progD;
					cmd="F8";
				}
				// TODO Auto-generated method stub
				
				for(int i=0; i<loopN;i++){
				ClientThread.setCommand(COMMAND_TOOL+"sendkeypress "+cmd);
				//Toast.makeText(getBaseContext(), Command, Toast.LENGTH_LONG).show();				
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
				}
				initProgress = progress;
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

		};
		audioSeekBarVLC.setOnSeekBarChangeListener(audioChangeListenerVLC);
		
		//Mute Toggle
		ToggleButton muteToggleButton = (ToggleButton) findViewById(R.id.toggleMaster);
		OnCheckedChangeListener muteListener = new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked)
					ClientThread.setCommand(COMMAND_TOOL+"mutesysvolume 1");
				else
					ClientThread.setCommand(COMMAND_TOOL+"mutesysvolume 0");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
			}};
			muteToggleButton.setOnCheckedChangeListener(muteListener);
		//VLC Toggle
			ToggleButton muteToggleButtonVLC = (ToggleButton) findViewById(R.id.toggleVLC);
			OnCheckedChangeListener muteListenerVLC = new OnCheckedChangeListener(){

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					// TODO Auto-generated method stub
					if(isChecked)
					{
					ClientThread.setCommand(COMMAND_TOOL+"sendkeypress F7");
					ClientThread.myThread = new Thread(new ClientThread());
					ClientThread.myThread.start();
					} 
					else{
						ClientThread.setCommand(COMMAND_TOOL+"sendkeypress F7");
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();
					}
				}};
				muteToggleButtonVLC.setOnCheckedChangeListener(muteListenerVLC);
		//START STOP REPLAY
				Button bStart  = (Button) findViewById(R.id.buttonStart);
				bStart.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+p");
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();		
					}
				});
				Button bStop  = (Button) findViewById(R.id.buttonStop);
				bStop.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+s");
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();		
					}
				});
				Button bReplay  = (Button) findViewById(R.id.buttonReplay);
				bReplay.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+s");
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();		

						ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+p");
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();
					}
				});
				
				//fast normal slow
				Button bFast  = (Button) findViewById(R.id.ButtonFast);
				bFast.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+shift+g");
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();		
					}
				});
				Button bSlow  = (Button) findViewById(R.id.Slow);
				bSlow.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+shift+s");
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();		
					}
				});
				Button bNormal  = (Button) findViewById(R.id.ButtonNormal);
				bNormal.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+shift+n");
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();		
					}
				});
				//control Toggle
				ToggleButton controlToggleButton = (ToggleButton) findViewById(R.id.toggleControls);
				OnCheckedChangeListener controlListener = new OnCheckedChangeListener(){

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						if(isChecked)
						ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+h");
//						else
//						ClientThread.setCommand(COMMAND_TOOL+"mutesysvolume 0");
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();
						
					}};
					controlToggleButton.setOnCheckedChangeListener(controlListener);
					//fullscreen Toggle
					ToggleButton fullToggleButton = (ToggleButton) findViewById(R.id.toggleFullscreen);
					OnCheckedChangeListener fullListener = new OnCheckedChangeListener(){

						@Override
						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							// TODO Auto-generated method stub
							if(isChecked)
							ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+t");
//							else
//							ClientThread.setCommand(COMMAND_TOOL+"mutesysvolume 0");
							ClientThread.myThread = new Thread(new ClientThread());
							ClientThread.myThread.start();
							
						}};
						fullToggleButton.setOnCheckedChangeListener(fullListener);
					//quitVLC	
						Button quitVLC  = (Button) findViewById(R.id.quitVLC);
						quitVLC.setOnClickListener(new View.OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								ClientThread.setCommand(COMMAND_TOOL+"sendkeypress alt+F4");
								ClientThread.myThread = new Thread(new ClientThread());
								ClientThread.myThread.start();		
							}
						});
						//Speed Track
						

//						final Spinner resSpinner = (Spinner) findViewById(R.id.spinnerJumpType);
//						ArrayList<String> res_spinner = new ArrayList<String>();
//						final ArrayList<String> res_command = new ArrayList<String>();
//						res_spinner.add("Very Short (3 Sec)");
//						res_command.add("shift+");
//						res_spinner.add("Short (10 Sec)");
//						res_command.add("alt+");
//						res_spinner.add("Medium");
//						res_command.add("ctrl+");
//						res_spinner.add("Long");
//						res_command.add("ctrl+alt+");
//					
//						ArrayAdapter resAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, res_spinner.toArray());
//						resSpinner.setAdapter(resAdapter);
//						resSpinner.setId(0);
						Button bFWD = (Button) findViewById(R.id.ButtonFwd); 
						bFWD.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+b");
								ClientThread.myThread = new Thread(new ClientThread());
								ClientThread.myThread.start();


							}
						});
						Button bBCK = (Button) findViewById(R.id.ButtonBack); 
						bBCK.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+f");
								ClientThread.myThread = new Thread(new ClientThread());
								ClientThread.myThread.start();


							}
						});
						Button bBring = (Button) findViewById(R.id.buttonBringWMP); 
						bBring.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								ClientThread.setCommand(COMMAND_TOOL+"exec max \"C:\\Program Files\\Windows Media Player\\wmplayer.exe\"");
								ClientThread.myThread = new Thread(new ClientThread());
								ClientThread.myThread.start();


							}
						});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.vlc, menu);
		return true;
	}

}
