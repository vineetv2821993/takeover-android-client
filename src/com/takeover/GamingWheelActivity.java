package com.takeover;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class GamingWheelActivity extends Activity implements SensorEventListener{

	
	private static final String Z = "26";
	private  static  final String X = "24";
	private  static  final String c = "3";
	private  static  final String V = "22";
	private  static  final String m = "13";    
	private  static  final String e = "5";
	private  static  final String r = "17";
	private static final String h = "8";
	private static final String alt = "28";
	private static final String spc = "27";
	private static final String NULL = "0";
	
    private static final String TAG = "WheelControl";
	public final String COMMAND_TOOL = "c:\\command\\nircmd ";

    private MjpegView mv;
	//
	protected static final String SWIPE_SCREEN = "100";

	private boolean mInitialized;

	private SensorManager mSensorManager;

	private Sensor mAccelerometer;

	private final float NOISE = (float) 0.5;
	private final String CMD_MOUSE = "c:\\command\\";
	
	//keys
	

	private final float alpha = (float) 0.8;
//	private float gravity[] = new float[3];
	private float x = (float) 0.0;
	private float y = (float) 0.0;
	private float z = (float) 0.0;
	private int jCommand = 0;
	private float X_NOISE = (float) 3.0;
	private float Y_NOISE = (float) 3.0;
	private final int UP = 65;
	private final int DOWN = -67;
	private final int LEFT = 66;
	private final int RIGHT = 68;
    private ImageView iv;
    private PowerManager pm;
    private PowerManager.WakeLock wl; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		    requestWindowFeature(Window.FEATURE_NO_TITLE);
	        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
	        WindowManager.LayoutParams.FLAG_FULLSCREEN);
	         pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
	         wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");

//	          ..screen will stay on during this section..
		setContentView(R.layout.activity_gaming_wheel);
		
		iv = (ImageView) findViewById(R.id.imageViewNavigation);
        mInitialized = false;
        
        ClientThread.SERVER_IP = IPInfo.getServerIP();

        Toast.makeText(getBaseContext(), "Key Configuration\n\nAccelerate: Tilt Up = Up Key\nBreak: Tilt Down = Down Key\nMove Left: Tilt Left\nMove Right: Tilt Right = Right Key", Toast.LENGTH_LONG).show();

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        
        
        
        Button Break = (Button) findViewById(R.id.buttonSpace); 
		Break.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ClientThread.setCommand(spc);
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();


			}
		});
        
        Button Nitrous = (Button) findViewById(R.id.buttonAlt); 
		Nitrous.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ClientThread.setCommand(alt);
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();


			}
		});
        Button pressZ = (Button) findViewById(R.id.wep1); 
		pressZ.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ClientThread.setCommand(Z);
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();


			}
		});
        Button pressX = (Button) findViewById(R.id.wep2); 
		pressX.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ClientThread.setCommand(X);
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();


			}
		});
	       Button pressC = (Button) findViewById(R.id.wep3); 
			pressC.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ClientThread.setCommand(c);
					ClientThread.myThread = new Thread(new ClientThread());
					ClientThread.myThread.start();


				}
			});
		       Button pressV = (Button) findViewById(R.id.wep4); 
				pressV.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ClientThread.setCommand(V);
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();


					}
				});
			       Button pressM = (Button) findViewById(R.id.change_music); 
					pressM.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							ClientThread.setCommand(m);
							ClientThread.myThread = new Thread(new ClientThread());
							ClientThread.myThread.start();


						}
					});
				       Button pressE = (Button) findViewById(R.id.change_car); 
						pressE.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								
								ClientThread.setCommand(e);
								ClientThread.myThread = new Thread(new ClientThread());
								ClientThread.myThread.start();


							}
						});
					       Button pressR = (Button) findViewById(R.id.change_reset); 
							pressR.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									ClientThread.setCommand(r);
									ClientThread.myThread = new Thread(new ClientThread());
									ClientThread.myThread.start();


								}
							});
						       Button pressH = (Button) findViewById(R.id.change_horn); 
								pressH.setOnClickListener(new View.OnClickListener() {

									@Override
									public void onClick(View v) {
										// TODO Auto-generated method stub
										ClientThread.setCommand(h);
										ClientThread.myThread = new Thread(new ClientThread());
										ClientThread.myThread.start();


									}
								});
			Button setX = (Button) findViewById(R.id.setX);
			setX.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					try{
					EditText etX = (EditText) findViewById(R.id.editTextX);	
					X_NOISE = Float.valueOf(etX.getText().toString());
				}
				catch(Exception e){
					Toast.makeText(getBaseContext(), "Enter Valid Value", Toast.LENGTH_LONG).show();
				}
					
				}
				
			});
			
			Button setY = (Button) findViewById(R.id.setY);
			setY.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try{
						EditText etY = (EditText) findViewById(R.id.EditTextY);	
					Y_NOISE = Float.valueOf(etY.getText().toString());			
					}
					catch(Exception e){
						Toast.makeText(getBaseContext(), "Enter Valid Value", Toast.LENGTH_LONG).show();
					}
				}
			});
			
			
	}
	
    protected void onResume() {

    	super.onResume();
//        wl.acquire();
        
        
    	mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

    	}
    protected void onPause() {

    	super.onPause();
//        wl.release();
    	mSensorManager.unregisterListener(this);

    	}
/////////////////////////
	@Override

	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	// can be safely ignored for this demo

	}
	
	@Override

	public void onSensorChanged(SensorEvent event) {

//	TextView tvX= (TextView)findViewById(R.id.x_axis);
//
//	TextView tvY= (TextView)findViewById(R.id.y_axis);
//
//	TextView tvZ= (TextView)findViewById(R.id.z_axis);

	//ImageView iv = (ImageView)findViewById(R.id.image);

	x = event.values[0];

	y = event.values[1];

	z = event.values[2];

	if (!mInitialized) {
//
//	tvX.setText("0.0");
//
//	tvY.setText("0.0");
//
//	tvZ.setText("0.0");

	mInitialized = true;

	} else {

		  // In this example, alpha is calculated as t / (t + dT),
		  // where t is the low-pass filter's time-constant and
		  // dT is the event delivery rate.


		  // Isolate the force of gravity with the low-pass filter.
//		  gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
//		  gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
//		  gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];

		  // Remove the gravity contribution with the high-pass filter.
		  x = event.values[0];// - gravity[0];
		  y = event.values[1];// - gravity[1];
		  z = event.values[2];// - gravity[2];
		  x = (float) ((Math.abs(x)>NOISE)?x:0.0);
		  y = (float) ((Math.abs(y)>NOISE)?y:0.0);
		  z = (float) ((Math.abs(z)>NOISE)?z:0.0);
//	tvX.setText(Float.toString(x));
//
//	tvY.setText(Float.toString(y));
//
//	tvZ.setText(Float.toString(z));
	//Executing Command
//	if(x>2.0){
//		ClientThread.setCommand("38");
//		ClientThread.myThread = new Thread(new ClientThread());
//		ClientThread.myThread.start();    		    		
//		
////		ClientThread.setCommand(CMD_MOUSE+"nircmd sendkey down press");
////		ClientThread.myThread = new Thread(new ClientThread());
////		ClientThread.myThread.start(); 
//	}
//	else if(x<-2.0){
//		ClientThread.setCommand("40");
//		ClientThread.myThread = new Thread(new ClientThread());
//		ClientThread.myThread.start();    		
////		ClientThread.setCommand(CMD_MOUSE+"nircmd sendkey up press");
////		ClientThread.myThread = new Thread(new ClientThread());
////		ClientThread.myThread.start(); 
//	}

		  ////	else{
////		ClientThread.setCommand(CMD_MOUSE+"nircmd sendkey down up");
////		ClientThread.myThread = new Thread(new ClientThread());
////		ClientThread.myThread.start();    		
////		ClientThread.setCommand(CMD_MOUSE+"nircmd sendkey up up");
////		ClientThread.myThread = new Thread(new ClientThread());
////		ClientThread.myThread.start();     		
////	}
//	if(y>3.0){
//		ClientThread.setCommand("37");
//		ClientThread.myThread = new Thread(new ClientThread());
//		ClientThread.myThread.start();    		
////		ClientThread.setCommand(CMD_MOUSE+"nircmd sendkey right up");
////		ClientThread.myThread = new Thread(new ClientThread());
////		ClientThread.myThread.start(); 
//	}
//	else if(y<-3.0){
//		ClientThread.setCommand("39");
//		ClientThread.myThread = new Thread(new ClientThread());
//		ClientThread.myThread.start();    		
////		ClientThread.setCommand(CMD_MOUSE+"nircmd sendkey left up");
////		ClientThread.myThread = new Thread(new ClientThread());
////		ClientThread.myThread.start(); 
//	}
	jCommand = 0;
	if(x>X_NOISE)
		jCommand = UP;
	else if(x<-X_NOISE)
		jCommand = DOWN;
	if(y>Y_NOISE)
		jCommand+= RIGHT;
	else if(y<-Y_NOISE)
		jCommand+= LEFT;
	if(jCommand!=0){
		ClientThread.setCommand(String.valueOf(jCommand));
		ClientThread.myThread = new Thread(new ClientThread());
		ClientThread.myThread.start(); 
		switch(jCommand){
		case UP: iv.setImageResource(R.drawable.up);
				 break;
		case DOWN: iv.setImageResource(R.drawable.down);
		 		 break;
		case RIGHT: iv.setImageResource(R.drawable.right);
		 		 break;
		case LEFT: iv.setImageResource(R.drawable.left);
		 	break;
		case UP+LEFT: iv.setImageResource(R.drawable.upleft);
		 	break;

		case UP+RIGHT: iv.setImageResource(R.drawable.upright);
		 	break; 

		case DOWN+LEFT: iv.setImageResource(R.drawable.downleft);
		 	break; 

		case DOWN+RIGHT: iv.setImageResource(R.drawable.downright);
		 	break; 
		default: 		iv.setImageResource(R.drawable.null_pos);
		 	break;
		 	
		}
	}else{
		ClientThread.setCommand(String.valueOf(NULL));
		ClientThread.myThread = new Thread(new ClientThread());
		ClientThread.myThread.start(); 
		iv.setImageResource(R.drawable.null_pos);
		}
//	else{
//		ClientThread.setCommand(CMD_MOUSE+"nircmd sendkey right up");
//		ClientThread.myThread = new Thread(new ClientThread());
//		ClientThread.myThread.start();    		
//		ClientThread.setCommand(CMD_MOUSE+"nircmd sendkey left up");
//		ClientThread.myThread = new Thread(new ClientThread());
//		ClientThread.myThread.start();     		
//	}

//	iv.setVisibility(View.VISIBLE);
//
////	if (deltaX > deltaY) {
////
////	iv.setImageResource(R.drawable.horizontal);
////
////	} else if (deltaY > deltaX) {
////
////	iv.setImageResource(R.drawable.vertical);
////
////	} else {
//
//	iv.setVisibility(View.INVISIBLE);

//	}

	}
}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.gaming_wheel, menu);
		return true;
	}

}
