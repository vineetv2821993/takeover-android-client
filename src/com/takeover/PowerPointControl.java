package com.takeover;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ZoomControls;

public class PowerPointControl extends Activity {
	
//	private  static  final int ZOOM_IN = 60;
//	private  static  final String ZOOM_OUT = "41";
	private static   final int CURSOR = 45;
	private static   final int TASKBAR = 55;
	public final String COMMAND_TOOL = "c:\\command\\nircmd ";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_power_point_control);
		Button prevSlide = (Button) findViewById(R.id.prevSlideButton); 
		prevSlide.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			ClientThread.setCommand(33+".ppt");
			ClientThread.myThread = new Thread(new ClientThread());
			ClientThread.myThread.start();
				
			}
		});
		Button nextSlide = (Button) findViewById(R.id.nextSlideButton); 
		nextSlide.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ClientThread.setCommand(34+".ppt");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
			}
		});
		Button jumpSlide = (Button) findViewById(R.id.jumpButton); 
		jumpSlide.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				EditText goP = (EditText) findViewById(R.id.slideNumEditText);
				ClientThread.setCommand(goP.getText().toString()+".ppt");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
			}
		});
		Button autoSlide = (Button) findViewById(R.id.autoButton); 
		autoSlide.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ClientThread.setCommand(83+".ppt");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
				
			}
		});
		Button taskBarView = (Button) findViewById(R.id.viewTaskButton); 
		taskBarView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ClientThread.setCommand(TASKBAR+".ppt");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
			}
		});
		ToggleButton blackToggleButton = (ToggleButton) findViewById(R.id.blackToggleButton);
		OnCheckedChangeListener blackListener = new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked)
					{
					ClientThread.setCommand(66+".ppt");
					
					ClientThread.myThread = new Thread(new ClientThread());
					ClientThread.myThread.start();
					}
			}};
			blackToggleButton.setOnCheckedChangeListener(blackListener);
			
			ToggleButton cursorToggleButton = (ToggleButton) findViewById(R.id.cursorToggleButton);
			OnCheckedChangeListener cursorListener = new OnCheckedChangeListener(){

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					// TODO Auto-generated method stub
					if(isChecked){
					ClientThread.setCommand(CURSOR+".ppt");
					ClientThread.myThread = new Thread(new ClientThread());
					ClientThread.myThread.start();
					}
				}};
				cursorToggleButton.setOnCheckedChangeListener(cursorListener);
			
			ZoomControls slideZoom = (ZoomControls) findViewById(R.id.zoomControl);
			slideZoom.setOnZoomInClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
				ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+plus");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
					
				}
			});
			slideZoom.setOnZoomOutClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
				ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+minus");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
				}
			});
			Button viewScreen  = (Button) findViewById(R.id.screenViewMode);
			viewScreen.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
//					Intent NextActivity = new Intent("com.takeover.MjpegActivity");
//					startActivity(NextActivity);
					Toast.makeText(getBaseContext(), "Curious??!!\nThis feature will be available soon in next available update!!", Toast.LENGTH_LONG).show();
				}
			});
			Button goTouch  = (Button) findViewById(R.id.touchPadView);
			goTouch.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent NextActivity = new Intent("com.takeover.TouchPadActivity");
					startActivity(NextActivity);			
				}
			});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.power_point_control, menu);
		return true;
	}

}
