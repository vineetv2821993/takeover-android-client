package com.takeover;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.takeover.MjpegActivity.DoRead;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class PlayerWMPActivity extends Activity {
	
	public static ImageButton bStart, bRepeat, bSuffle;
	public static boolean bStartToggle, bSuffleToggle = true, bRepeatToggle = true, speedChange = false;

	public final String COMMAND_TOOL = "c:\\command\\nircmd ";
	public static int initProgress = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		   requestWindowFeature(Window.FEATURE_NO_TITLE);
	        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
	        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		ClientThread.setCommand(COMMAND_TOOL+"exec max \"C:\\Program Files\\Windows Media Player\\wmplayer.exe\"");
		ClientThread.myThread = new Thread(new ClientThread());
		ClientThread.myThread.start();
		setContentView(R.layout.wmp_player);
		
		//Master Control

		SeekBar audioSeekBar = (SeekBar) findViewById(R.id.seekBarMaster);
		audioSeekBar.setMax(65535);
		OnSeekBarChangeListener audioChangeListener = new OnSeekBarChangeListener(){

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				ClientThread.setCommand(COMMAND_TOOL+"setsysvolume "+progress);
				//Toast.makeText(getBaseContext(), Command, Toast.LENGTH_LONG).show();				
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

		};
		audioSeekBar.setOnSeekBarChangeListener(audioChangeListener);
		
		//VLC Control
		

		SeekBar audioSeekBarVLC = (SeekBar) findViewById(R.id.seekBarWMP);
		audioSeekBarVLC.setMax(10);

		OnSeekBarChangeListener audioChangeListenerVLC = new OnSeekBarChangeListener(){

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				int progD = progress - initProgress;
				int loopN = 0;
				String cmd = "";
				if(progress == 0){
					loopN=16;
					cmd="F8";
				}
				else if(progress == 10){
					loopN=10;
					cmd="F9";
				}
				else if(progD>0){
					loopN=progD;
					cmd="F9";
				}
				else if(progD<0){
					loopN=-progD;
					cmd="F8";
				}
				// TODO Auto-generated method stub
				
				for(int i=0; i<loopN;i++){
				ClientThread.setCommand(COMMAND_TOOL+"sendkeypress "+cmd);
				//Toast.makeText(getBaseContext(), Command, Toast.LENGTH_LONG).show();				
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
				}
				initProgress = progress;
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

		};
		audioSeekBarVLC.setOnSeekBarChangeListener(audioChangeListenerVLC);
		
		//Mute Toggle
		ToggleButton muteToggleButton = (ToggleButton) findViewById(R.id.toggleButtonMaster);
		OnCheckedChangeListener muteListener = new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked)
					ClientThread.setCommand(COMMAND_TOOL+"mutesysvolume 1");
				else
					ClientThread.setCommand(COMMAND_TOOL+"mutesysvolume 0");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
			}};
			muteToggleButton.setOnCheckedChangeListener(muteListener);
		//VLC Toggle
			ToggleButton muteToggleButtonVLC = (ToggleButton) findViewById(R.id.toggleButtonWMP);
			OnCheckedChangeListener muteListenerVLC = new OnCheckedChangeListener(){

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					// TODO Auto-generated method stub
					if(isChecked)
					{
					ClientThread.setCommand(COMMAND_TOOL+"sendkeypress F7");
					ClientThread.myThread = new Thread(new ClientThread());
					ClientThread.myThread.start();
					} 
					else{
						ClientThread.setCommand(COMMAND_TOOL+"sendkeypress F7");
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();
					}
				}};
				muteToggleButtonVLC.setOnCheckedChangeListener(muteListenerVLC);
		//START STOP REPLAY
				bStart  = (ImageButton) findViewById(R.id.imageButtonPlay);
			    bStartToggle = true;
				bStart.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
	
						ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+p");
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();		
						if(bStartToggle == true){
						bStart.setImageResource(R.drawable.pause);
						bStartToggle = false;
						}
						else{
							bStart.setImageResource(R.drawable.play);
							bStartToggle = true;
						}
					}
				});
				ImageButton bStop  = (ImageButton) findViewById(R.id.imageButtonStop);
				bStop.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(speedChange==true){
							ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+shift+n");
							ClientThread.myThread = new Thread(new ClientThread());
								ClientThread.myThread.start();
								speedChange = false;
						}else{						ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+s");
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();
						speedChange = true;
						}
					}
				});
//				Button bReplay  = (Button) findViewById(R.id.buttonReplay);
//				bReplay.setOnClickListener(new View.OnClickListener() {
//					
//					@Override
//					public void onClick(View v) {
//						// TODO Auto-generated method stub
//						ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+s");
//						ClientThread.myThread = new Thread(new ClientThread());
//						ClientThread.myThread.start();		
//
//						ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+p");
//						ClientThread.myThread = new Thread(new ClientThread());
//						ClientThread.myThread.start();
//					}
//				});
				
				//fast normal slow
				ImageButton bFast  = (ImageButton) findViewById(R.id.imageButtonFast);
				bFast.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(speedChange==true){
							ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+shift+n");
							ClientThread.myThread = new Thread(new ClientThread());
								ClientThread.myThread.start();
								speedChange = false;
						}else{	
						ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+shift+g");
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();
						speedChange = true;
						}
					}
				});
				ImageButton bSlow  = (ImageButton) findViewById(R.id.imageButtonSlow);
				bSlow.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+shift+s");
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();	
						speedChange = true;
					}
				});
//				Button bNormal  = (Button) findViewById(R.id.ButtonNormal);
//				bNormal.setOnClickListener(new View.OnClickListener() {
//					
//					@Override
//					public void onClick(View v) {
//						// TODO Auto-generated method stub
//						ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+shift+n");
//						ClientThread.myThread = new Thread(new ClientThread());
//						ClientThread.myThread.start();		
//					}
//				});
				//Suffle Toggle
				bSuffle  = (ImageButton) findViewById(R.id.imageButtonSuffle);
				bSuffle.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+h");
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();	
						if(bSuffleToggle == true){
						bSuffle.setImageResource(R.drawable.suffleon);
						bSuffleToggle = false;
						}
						else{
							bSuffle.setImageResource(R.drawable.suffleoff);
							bSuffleToggle = true;
						}
					}
				});
				//Repeat Toggle
				bRepeat  = (ImageButton) findViewById(R.id.imageButtonRepeat);
				bRepeat.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+t");
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();		
						if(bRepeatToggle == true){
						bRepeat.setImageResource(R.drawable.repeaton);
						bRepeatToggle = false;
						}
						else{
							bRepeat.setImageResource(R.drawable.repeatoff);
							bRepeatToggle = true;
						}
					}
				});
					//quitVLC	
						Button quitVLC  = (Button) findViewById(R.id.buttonQuitWMP);
						quitVLC.setOnClickListener(new View.OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								ClientThread.setCommand("taskkill /f /im wmplayer.exe");
								ClientThread.myThread = new Thread(new ClientThread());
								ClientThread.myThread.start();		
							}
						});
						//Speed Track
						

//						final Spinner resSpinner = (Spinner) findViewById(R.id.spinnerJumpType);
//						ArrayList<String> res_spinner = new ArrayList<String>();
//						final ArrayList<String> res_command = new ArrayList<String>();
//						res_spinner.add("Very Short (3 Sec)");
//						res_command.add("shift+");
//						res_spinner.add("Short (10 Sec)");
//						res_command.add("alt+");
//						res_spinner.add("Medium");
//						res_command.add("ctrl+");
//						res_spinner.add("Long");
//						res_command.add("ctrl+alt+");
//					
//						ArrayAdapter resAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, res_spinner.toArray());
//						resSpinner.setAdapter(resAdapter);
//						resSpinner.setId(0);
						ImageButton bFWD = (ImageButton) findViewById(R.id.imageButtonNext); 
						bFWD.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+b");
								ClientThread.myThread = new Thread(new ClientThread());
								ClientThread.myThread.start();


							}
						});
						ImageButton bBCK = (ImageButton) findViewById(R.id.imageButtonPrev); 
						bBCK.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+f");
								ClientThread.myThread = new Thread(new ClientThread());
								ClientThread.myThread.start();


							}
						});
						Button bBring = (Button) findViewById(R.id.buttonBringWMPF); 
						bBring.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								ClientThread.setCommand(COMMAND_TOOL+"exec max \"C:\\Program Files\\Windows Media Player\\wmplayer.exe\"");
								ClientThread.myThread = new Thread(new ClientThread());
								ClientThread.myThread.start();


							}
						});
//					wmpView = (MjpegView) findViewById(R.id.mjpegViewWMP);
//			        String URL = "http://"+IPInfo.getServerIP()+":12120/";
//			        new DoRead().execute(URL);

	}
/*	MjpegView wmpView ;
    private static final String TAG = "MjpegActivity";
	 public class DoRead extends AsyncTask<String, Void, MjpegInputStream> {
	        protected MjpegInputStream doInBackground(String... url) {
	            //TODO: if camera has authentication deal with it and don't just not work
	            HttpResponse res = null;
	            DefaultHttpClient httpclient = new DefaultHttpClient();     
	            Log.d(TAG, "1. Sending http request");
	            try {
	                res = httpclient.execute(new HttpGet(URI.create(url[0])));
	                Log.d(TAG, "2. Request finished, status = " + res.getStatusLine().getStatusCode());
	                if(res.getStatusLine().getStatusCode()==401){
	                    //You must turn off camera User Access Control before this will work
	                    return null;
	                }
	                return new MjpegInputStream(res.getEntity().getContent());  
	            } catch (ClientProtocolException e) {
	                e.printStackTrace();
	                Log.d(TAG, "Request failed-ClientProtocolException", e);
	                //Error connecting to camera
	            } catch (IOException e) {
	                e.printStackTrace();
	                Log.d(TAG, "Request failed-IOException", e);
	                //Error connecting to camera
	            }

	            return null;
	        }

	        protected void onPostExecute(MjpegInputStream result) {
	            wmpView.setSource(result);
	            wmpView.setDisplayMode(MjpegView.SIZE_BEST_FIT);
	            wmpView.showFps(false);
	        }
	    }
	    public void onPause() {
	        super.onPause();
	        wmpView.stopPlayback();
	    }
*/

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.vlc, menu);
		return true;
	}

}
