package com.takeover;

import java.util.ArrayList;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Client extends Activity {

	//private Socket socket;
	//	private static String Command;
	//	private static int I;
	//	public static void setCommand(COMMAND_TOOL+String cmd){
	//		Command = cmd;
	//	}
	//	public static String getCommand(){
	//		return Command;
	//	}

	//	private static final int SERVERPORT = 12128;
	private static final int RESULT_CLOSE_ALL = 1000;
	protected static final String AIR_MOUSE_ACTIVITY = "com.takeover.MainActivity";
	protected static final String MOUSE_ACTIVITY = "com.takeover.TouchPadActivity";
	protected static final String TERMINAL_ACTIVITY = "com.takeover.TerminalActivity";
	protected static final String WHEEL_CONTROL = "com.takeover.GamingWheelActivity";
	protected static final String SNAPSHOT_ACTIVITY = "com.takeover.SnapshotActivity";
	protected static final String REMOTE_ACTIVITY = "com.takeover.RemoteActivity";
	protected static final String MJPEG_ACTIVITY = "com.takeover.MjpegActivity";

	protected static final String WMP_CONTROL = "com.takeover.WMPActivity";
	protected static final String PWMP_CONTROL = "com.takeover.PlayerWMPActivity";
	protected static final String VLC_CONTROL = "com.takeover.VLCActivity";
	protected static final String PPT_CONTROL = "com.takeover.PowerPointControl";
	protected static final String NAV_PANEL = "com.takeover.NavigationPanel";
	protected static final String AUDIO_MIC = "com.takeover.Send";
	protected static final String TOUCH_SCREEN = "com.takeover.TouchScreen";
	protected static final String TOUCHSCREEN_ACTIVITY = "com.takeover.TouchScreenActivity";
	
	protected static final String SWIPE_SCREEN = "100";
	public final String COMMAND_TOOL = "c:\\command\\nircmd ";
	//	private static String SERVER_IP;
	//	public static Thread myThread;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.control_room);

		ClientThread.SERVER_IP = IPInfo.getServerIP();

		ClientThread.setCommand("c:\\command\\nircmd trayballoon \"TakeOver Server\" \"Client is Connected\" \"shell32.dll,22\" 15000");				
		ClientThread.myThread = new Thread(new ClientThread());
		ClientThread.myThread.start();
		
		Toast.makeText(getBaseContext(), ClientThread.SERVER_IP+" "+HttpThread.RESPONSE, Toast.LENGTH_LONG).show();

		//ScrollView addScrollView = (ScrollView) findViewById(R.id.addScrollView);
		//RelativeLayout otherControl = (RelativeLayout) findViewById(R.layout.othercontrol);
		//addScrollView.addView(otherControl);		
		/*	//Terminal Command
		Button ClickSend = (Button) findViewById(R.id.terminalCmd);
		ClickSend.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				EditText et = (EditText) findViewById(R.id.terminalEditText);
				Command = et.getText().toString();
				myThread = new Thread(new ClientThread());
				myThread.start();
			}
		});
		 */

		//Audio Control

		SeekBar audioSeekBar = (SeekBar) findViewById(R.id.audioSeekBar);
		audioSeekBar.setMax(65535);
		OnSeekBarChangeListener audioChangeListener = new OnSeekBarChangeListener(){

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				ClientThread.setCommand(COMMAND_TOOL+"setsysvolume "+progress);
				//Toast.makeText(getBaseContext(), Command, Toast.LENGTH_LONG).show();				
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

		};
		audioSeekBar.setOnSeekBarChangeListener(audioChangeListener);


		//Brightness Control
		SeekBar brightnessSeekBar = (SeekBar) findViewById(R.id.brightnessSeekBar);
		brightnessSeekBar.setMax(100);
		OnSeekBarChangeListener brightnessChangeListener = new OnSeekBarChangeListener(){

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				ClientThread.setCommand(COMMAND_TOOL+"setbrightness "+progress);
				//Toast.makeText(getBaseContext(), Command, Toast.LENGTH_LONG).show();				
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

		};
		brightnessSeekBar.setOnSeekBarChangeListener(brightnessChangeListener);

		//Mute Toggle
		ToggleButton muteToggleButton = (ToggleButton) findViewById(R.id.muteToggleButton);
		OnCheckedChangeListener muteListener = new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked)
					ClientThread.setCommand(COMMAND_TOOL+"mutesysvolume 1");
				else
					ClientThread.setCommand(COMMAND_TOOL+"mutesysvolume 0");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
			}};
			muteToggleButton.setOnCheckedChangeListener(muteListener);


			//CD ROM Toggle
			ToggleButton cdToggleButton = (ToggleButton) findViewById(R.id.cdToggleButton);
			OnCheckedChangeListener cdListener = new OnCheckedChangeListener(){

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					// TODO Auto-generated method stub
					if(isChecked)
						ClientThread.setCommand(COMMAND_TOOL+"cdrom open");
					else
						ClientThread.setCommand(COMMAND_TOOL+"cdrom close");
					ClientThread.myThread = new Thread(new ClientThread());
					ClientThread.myThread.start();
				}};
				cdToggleButton.setOnCheckedChangeListener(cdListener);

				//Recycle Bin Clear		
				Button binClear = (Button) findViewById(R.id.emptyBinButton);
				binClear.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ClientThread.setCommand(COMMAND_TOOL+"emptybin");
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();
					}
				});	
				//Speak Clipboard		
				Button speakCB = (Button) findViewById(R.id.speakClipboard);
				speakCB.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ClientThread.setCommand(COMMAND_TOOL+"speak text ~$clipboard$");
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();
					}
				});	
				//clipboard clear 		
				Button clearCB = (Button) findViewById(R.id.clearClipboard);
				clearCB.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ClientThread.setCommand(COMMAND_TOOL+"clipboard clear");
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();
					}
				});
				//System Spinner
				final Spinner sysSpinner = (Spinner) findViewById(R.id.systemSpinner);
				ArrayList<String> array_spinner = new ArrayList<String>();
				final ArrayList<String> spinner_command = new ArrayList<String>();
				array_spinner.add("Turn Off Display");
				spinner_command.add("monitor off");
				array_spinner.add("Turn On Display");
				spinner_command.add("monitor on");
				array_spinner.add("Screensaver");
				spinner_command.add("screensaver");
				array_spinner.add("Log Off");
				spinner_command.add("exitwin logoff");
				array_spinner.add("Hibernate");
				spinner_command.add("hibernate ");
				array_spinner.add("Reboot");
				spinner_command.add("exitwin reboot");
				array_spinner.add("Power Off");
				spinner_command.add("exitwin poweroff");
				array_spinner.add("Shut Down");
				spinner_command.add("exitwin shutdown");
				array_spinner.add("Force Log Off");
				spinner_command.add("exitwin logoff forceifhung");
				array_spinner.add("Force Hibernate");
				spinner_command.add("hibernate force");
				array_spinner.add("Force Shut Down");
				spinner_command.add("exitwin shutdownforce");
				array_spinner.add("Force Power Off");
				spinner_command.add("exitwin poweroff force");



				ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, array_spinner.toArray());
				sysSpinner.setAdapter(adapter);
				sysSpinner.setId(0);
				Button sysSpinnerDone = (Button) findViewById(R.id.sysSpinnerDone); 
				sysSpinnerDone.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ClientThread.setCommand(COMMAND_TOOL+spinner_command.get(sysSpinner.getFirstVisiblePosition()));
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();


					}
				});
				//Resolution Spinner

				final Spinner resSpinner = (Spinner) findViewById(R.id.resSpinner);
				ArrayList<String> res_spinner = new ArrayList<String>();
				final ArrayList<String> res_command = new ArrayList<String>();
				res_spinner.add("800x600 16 Bit");
				res_command.add("setdisplay 800 600 16");
				res_spinner.add("800x600 32 Bit");
				res_command.add("setdisplay 800 600 32");
				res_spinner.add("1024x768 16 Bit");
				res_command.add("setdisplay 1024 768 16");
				res_spinner.add("1024x768 32 Bit");
				res_command.add("setdisplay 1024 768 32");
				res_spinner.add("1280x720 32 Bit HD");
				res_command.add("setdisplay 1280 720 32");
				res_spinner.add("1366x768 32 Bit HD");
				res_command.add("setdisplay 1366 768 32");
				res_spinner.add("1920x1080 32 Bit FULL HD");
				res_command.add("setdisplay 1920 1080 32");

				ArrayAdapter resAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, res_spinner.toArray());
				resSpinner.setAdapter(resAdapter);
				resSpinner.setId(0);
				Button resSpinnerDone = (Button) findViewById(R.id.resButton); 
				resSpinnerDone.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						ClientThread.setCommand(COMMAND_TOOL+res_command.get(resSpinner.getFirstVisiblePosition()));
						ClientThread.myThread = new Thread(new ClientThread());
						ClientThread.myThread.start();


					}
				});
				// Specific Application Controller Spinner
				//System Spinner
				final Spinner softappSpinner = (Spinner) findViewById(R.id.appcontrollerSpinner);
				ArrayList<String> softapp_spinner = new ArrayList<String>();
				final ArrayList<String> softapp_command = new ArrayList<String>();
				softapp_spinner.add("Nav-Panel");
				softapp_command.add(NAV_PANEL);//UnAvaialable
				softapp_spinner.add("WMP Player");
				softapp_command.add(PWMP_CONTROL);//UnAvaialable
				softapp_spinner.add("VLC");
				softapp_command.add("TOAST");//UnAvaialable
				//softapp_command.add(VLC_CONTROL);//UnAvaialable
				softapp_spinner.add("Powerpoint");//Avaialable
				softapp_command.add(PPT_CONTROL);
			
				softapp_spinner.add("My Computer");//Avaialable
				softapp_command.add("TOAST");
				softapp_spinner.add("Camera To Desktop");
				softapp_command.add("TOAST");//UnAvaialable

				ArrayAdapter softappAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, softapp_spinner.toArray());
				softappSpinner.setAdapter(softappAdapter);
				softappSpinner.setId(0);
			//	Context IPInfoContext = 
				Button softappSpinnerDone = (Button) findViewById(R.id.goHardButton); 
				softappSpinnerDone.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						//				ClientThread.setCommand(COMMAND_TOOL+spinner_command.get(sysSpinner.getFirstVisiblePosition()));
						//				ClientThread.myThread = new Thread(new ClientThread());
						//				ClientThread.myThread.start();
						//				
						// Intent Change Command
						if(!softapp_command.get(softappSpinner.getFirstVisiblePosition()).equals("TOAST")){
							Intent NextActivity = new Intent(softapp_command.get(softappSpinner.getFirstVisiblePosition()));
							startActivity(NextActivity);
						}
						else{
							
							Toast.makeText(getBaseContext(), "Wait!! This feature will be available soon in next available update!!", Toast.LENGTH_LONG).show();
						}

					}
				});
				//  Hardware Control Spinner

				//System Spinner
				final Spinner hardappSpinner = (Spinner) findViewById(R.id.hardcontrolSpinner);
				ArrayList<String> hardapp_spinner = new ArrayList<String>();
				final ArrayList<String> hardapp_command = new ArrayList<String>();
				hardapp_spinner.add("Air Mouse");//Available
				hardapp_command.add(AIR_MOUSE_ACTIVITY);
				hardapp_spinner.add("Gaming Wheel");//UnAvailable
				//hardapp_command.add("TOAST");
				hardapp_command.add(WHEEL_CONTROL);
				hardapp_spinner.add("Shooter Mode");//UnAvailable
				hardapp_command.add("TOAST");
				hardapp_spinner.add("Game Pad");//UnAvailable
				hardapp_command.add("TOAST");
//				hardapp_command.add(WHEEL_CONTROL);
				hardapp_spinner.add("Screen Monitoring");//UnAvaialble
				//hardapp_command.add("TOAST");			
				hardapp_command.add(MJPEG_ACTIVITY);
				
				hardapp_spinner.add("Touch Screen");//UnAvaialble
				//hardapp_command.add("TOAST");			
				hardapp_command.add(TOUCH_SCREEN);
				hardapp_spinner.add("Touch Screen 2");//UnAvaialble
				//hardapp_command.add("TOAST");			
				hardapp_command.add(TOUCHSCREEN_ACTIVITY);
				
				hardapp_spinner.add("Microphone");//UnAvaialble
				hardapp_command.add(AUDIO_MIC);		

				hardapp_spinner.add("Terminal/Shell");//Available
		//		hardapp_command.add(TERMINAL_ACTIVITY);
				hardapp_command.add("TOAST");
				hardapp_spinner.add("Touchpad");//Available
				hardapp_command.add(MOUSE_ACTIVITY);
				hardapp_spinner.add("Remote Desktop");//Available
				hardapp_command.add("TOAST");
				hardapp_spinner.add("Keyboard");//UnAvailable
				hardapp_command.add("TOAST");


				ArrayAdapter hardappAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, hardapp_spinner.toArray());
				hardappSpinner.setAdapter(hardappAdapter);
				sysSpinner.setId(0);
				Button hardappSpinnerDone = (Button) findViewById(R.id.goButton); 
				hardappSpinnerDone.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						//				ClientThread.setCommand(COMMAND_TOOL+spinner_command.get(sysSpinner.getFirstVisiblePosition()));
						//				ClientThread.myThread = new Thread(new ClientThread());
						//				ClientThread.myThread.start();

						if(!hardapp_command.get(hardappSpinner.getFirstVisiblePosition()).equals("TOAST")){
							Intent NextActivity = new Intent(hardapp_command.get(hardappSpinner.getFirstVisiblePosition()));
							startActivity(NextActivity);
					}
						else{
							Toast.makeText(getBaseContext(), "Wait!! This feature will be available soon in next available update!!", Toast.LENGTH_LONG).show();
						}

					}
				});

						//Button Open PAd Less mouse Activity
						}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(resultCode)
		{
		case RESULT_CLOSE_ALL:
			setResult(RESULT_CLOSE_ALL);
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	//	class ClientThread implements Runnable {
	//
	//		@Override
	//		public void run() {
	//			
	//			try {
	//				InetAddress serverAddr = InetAddress.getByName(SERVER_IP);
	//
	//				socket = new Socket(serverAddr, SERVERPORT);
	//				
	//
	//				PrintWriter out = new PrintWriter(new BufferedWriter(
	//						new OutputStreamWriter(socket.getOutputStream())),
	//						true);
	//				//Toast.makeText(getBaseContext(), Client.Command, Toast.LENGTH_LONG).show();
	//				out.println(Client.getCommand());
	//				socket.close();
	//
	//			} catch (UnknownHostException e1) {
	//				Toast.makeText(getBaseContext(), "ErrorD: "+e1.toString(), Toast.LENGTH_LONG).show();
	//				e1.printStackTrace();
	//			} catch (IOException e1) {
	//				Toast.makeText(getBaseContext(), "ErrorE: "+e1.toString(), Toast.LENGTH_LONG).show();
	//				e1.printStackTrace();
	//			}
	//
	//		}
	//
	//	}
}