package com.takeover;


//import it.sephiroth.android.wheel.view.Wheel;
//import it.sephiroth.android.wheel.view.Wheel.OnScrollListener;


import it.sephiroth.android.wheel.view.Wheel;
import android.annotation.SuppressLint;
import android.app.Activity;

import android.content.Context;

import android.hardware.Sensor;

import android.hardware.SensorEvent;

import android.hardware.SensorEventListener;

import android.hardware.SensorManager;

import android.os.Bundle;

import android.view.View;

import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Toast;

import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;

@SuppressLint("NewApi")
public class MainActivity extends Activity implements SensorEventListener{
	

	private boolean mInitialized;

	private SensorManager mSensorManager;

	private Sensor mAccelerometer;

	private final float NOISE = (float) 0.2;
	private final String CMD_MOUSE = "c:\\command\\nircmd movecursor ";
	private final String CMD_PROMPT = "c:\\command\\";
	private final float alpha = (float) 0.8;
	private float gravity[] = new float[3];
	private float linear_acceleration[] = new float[3];
	int prev = 0;
//	private static Wheel mouseWheel;
    @SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        RadioButton upScroll = (RadioButton) findViewById(R.id.radioUp);
//        RadioButton downScroll = (RadioButton) findViewById(R.id.radioDown);
       // RadioButton nullScroll = (RadioButton) findViewById(R.id.radioNull);
//		OnCheckedChangeListener upCheckListen = new OnCheckedChangeListener(){
//
//			@Override
//			public void onCheckedChanged(CompoundButton buttonView,
//					boolean isChecked) {
//				// TODO Auto-generated method stub
//				if(isChecked){
//					ClientThread.setCommand(CMD_PROMPT+"nircmd sendmouse wheel 600");
//					ClientThread.myThread = new Thread(new ClientThread());
//					ClientThread.myThread.start();
//				}
//			}};
//        upScroll.setOnCheckedChangeListener(upCheckListen);
//        
//		OnCheckedChangeListener downCheckListen = new OnCheckedChangeListener(){
//
//			@Override
//			public void onCheckedChanged(CompoundButton buttonView,
//					boolean isChecked) {
//				// TODO Auto-generated method stub
//				if(isChecked){
//					ClientThread.setCommand(CMD_PROMPT+"nircmd sendmouse wheel -600");
//					ClientThread.myThread = new Thread(new ClientThread());
//					ClientThread.myThread.start();
//				}
//			}};
//        downScroll.setOnCheckedChangeListener(downCheckListen);
        Wheel mouseWheel = (Wheel) findViewById(R.id.wheelMouse);
        mouseWheel.setWheelScaleFactor(12);
        mouseWheel.setOnScrollListener(new Wheel.OnScrollListener() {
			
			@Override
			public void onScrollStarted(Wheel view, float value, int roundValue) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onScrollFinished(Wheel view, float value, int roundValue) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onScroll(Wheel view, float value, int roundValue) {
				// TODO Auto-generated method stub
				ClientThread.setCommand(CMD_PROMPT+"nircmd sendmouse wheel "+(prev-roundValue)*60);
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
				prev = roundValue;
			}
		});
        
        
        mInitialized = false;
        
        ClientThread.SERVER_IP = IPInfo.getServerIP();
		Toast.makeText(getBaseContext(), ClientThread.SERVER_IP, Toast.LENGTH_LONG).show();

		Button leftClick = (Button) findViewById(R.id.buttonLeft);
		Button rightClick = (Button) findViewById(R.id.buttonRight);
//		mouseWheel = (Wheel) findViewById(R.id.wheel);
		leftClick.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ClientThread.setCommand(CMD_PROMPT+"nircmd sendmouse left click");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
				
			}
			
		});
		
		rightClick.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ClientThread.setCommand(CMD_PROMPT+"nircmd sendmouse right click");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
				
			}
			
		});
		
//		mouseWheel.setOnScrollListener( new OnScrollListener() {
//
//	        @Override
//	        public void onScrollStarted( Wheel view, float value, int roundValue ) {
//	        }
//
//	        @Override
//	        public void onScrollFinished( Wheel view, float value, int roundValue ) {
//	        }
//
//	        @Override
//	        public void onScroll( Wheel view, float value, int roundValue ) {
//				ClientThread.setCommand(CMD_PROMPT+"nircmd sendmouse wheel "+roundValue);
//				ClientThread.myThread = new Thread(new ClientThread());
//				ClientThread.myThread.start();
//	        }
//	    } );
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }
    protected void onResume() {

    	super.onResume();

    	mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

    	}

    	protected void onPause() {

    	super.onPause();

    	mSensorManager.unregisterListener(this);

    	}

    	@Override

    	public void onAccuracyChanged(Sensor sensor, int accuracy) {

    	// can be safely ignored for this demo

    	}
    	
    	@Override

    	public void onSensorChanged(SensorEvent event) {

//    	TextView tvX= (TextView)findViewById(R.id.x_axis);
//
//    	TextView tvY= (TextView)findViewById(R.id.y_axis);
//
//    	TextView tvZ= (TextView)findViewById(R.id.z_axis);

    	//ImageView iv = (ImageView)findViewById(R.id.image);


    	if (!mInitialized) {

//    	tvX.setText("0.0");
//
//    	tvY.setText("0.0");
//
//    	tvZ.setText("0.0");

    	mInitialized = true;

    	} else {

    		  // In this example, alpha is calculated as t / (t + dT),
    		  // where t is the low-pass filter's time-constant and
    		  // dT is the event delivery rate.


    		  // Isolate the force of gravity with the low-pass filter.
    		  gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
    		  gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
    		  gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];

    		  // Remove the gravity contribution with the high-pass filter.
    		  linear_acceleration[0] = event.values[0] - gravity[0];
    		  linear_acceleration[1] = event.values[1] - gravity[1];
    		  linear_acceleration[2] = event.values[2] - gravity[2];
    		  linear_acceleration[0] = -(float) ((Math.abs(linear_acceleration[0])>NOISE)?linear_acceleration[0]:0.0);
    		  linear_acceleration[1] = -(float) ((Math.abs(linear_acceleration[1])>NOISE)?linear_acceleration[1]:0.0);
    		  linear_acceleration[2] = (float) ((Math.abs(linear_acceleration[2])>NOISE)?linear_acceleration[2]:0.0);
//    	tvX.setText(Float.toString(linear_acceleration[0]));
//
//    	tvY.setText(Float.toString(linear_acceleration[1]));
//
//    	tvZ.setText(Float.toString(linear_acceleration[2]));
    	//Executing Command
		ClientThread.setCommand(CMD_MOUSE+(int)((linear_acceleration[0])*40)+" "+(int)((linear_acceleration[1])*30));
		ClientThread.myThread = new Thread(new ClientThread());
		ClientThread.myThread.start();

    	//iv.setVisibility(View.VISIBLE);

//    	if (deltaX > deltaY) {
//
//    	iv.setImageResource(R.drawable.horizontal);
//
//    	} else if (deltaY > deltaX) {
//
//    	iv.setImageResource(R.drawable.vertical);
//
//    	} else {

    	//iv.setVisibility(View.INVISIBLE);

//    	}

    	}

    	}

    
}
