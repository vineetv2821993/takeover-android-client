/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.takeover;

public final class R {
    public static final class attr {
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int buttonBarButtonStyle=0x7f010001;
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int buttonBarStyle=0x7f010000;
    }
    public static final class color {
        public static final int black_overlay=0x7f040000;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f050000;
        public static final int activity_vertical_margin=0x7f050001;
    }
    public static final class drawable {
        public static final int backspace=0x7f020000;
        public static final int bwd=0x7f020001;
        public static final int down=0x7f020002;
        public static final int downleft=0x7f020003;
        public static final int downright=0x7f020004;
        public static final int end=0x7f020005;
        public static final int enter=0x7f020006;
        public static final int esc=0x7f020007;
        public static final int fwd=0x7f020008;
        public static final int home=0x7f020009;
        public static final int ic_launcher=0x7f02000a;
        public static final int image=0x7f02000b;
        public static final int left=0x7f02000c;
        public static final int mouse=0x7f02000d;
        public static final int navdown=0x7f02000e;
        public static final int navleft=0x7f02000f;
        public static final int navright=0x7f020010;
        public static final int navup=0x7f020011;
        public static final int next=0x7f020012;
        public static final int null_pos=0x7f020013;
        public static final int pause=0x7f020014;
        public static final int play=0x7f020015;
        public static final int prev=0x7f020016;
        public static final int repeatoff=0x7f020017;
        public static final int repeaton=0x7f020018;
        public static final int right=0x7f020019;
        public static final int setipim=0x7f02001a;
        public static final int stop=0x7f02001b;
        public static final int suffleoff=0x7f02001c;
        public static final int suffleon=0x7f02001d;
        public static final int tab=0x7f02001e;
        public static final int up=0x7f02001f;
        public static final int upleft=0x7f020020;
        public static final int upright=0x7f020021;
    }
    public static final class id {
        public static final int AbsoluteLayout1=0x7f09001d;
        public static final int ButtonBack=0x7f090052;
        public static final int ButtonFast=0x7f09004d;
        public static final int ButtonFwd=0x7f090051;
        public static final int ButtonNormal=0x7f09004c;
        public static final int EditTextY=0x7f090012;
        public static final int Slow=0x7f09004e;
        public static final int TableLayout1=0x7f090059;
        public static final int TableRow01=0x7f09000e;
        public static final int TableRow02=0x7f090011;
        public static final int TableRow03=0x7f09004f;
        public static final int TextView01=0x7f090047;
        public static final int action_settings=0x7f09008b;
        public static final int appcontrollerSpinner=0x7f09006a;
        public static final int audioControl=0x7f09005a;
        public static final int audioSeekBar=0x7f09005b;
        public static final int autoButton=0x7f090036;
        public static final int blackToggleButton=0x7f090033;
        public static final int brightness=0x7f09005d;
        public static final int brightnessSeekBar=0x7f09005e;
        public static final int buttonAlt=0x7f090002;
        public static final int buttonBringWMP=0x7f090056;
        public static final int buttonBringWMPF=0x7f090080;
        public static final int buttonLeft=0x7f090017;
        public static final int buttonQuitWMP=0x7f09008a;
        public static final int buttonReplay=0x7f09004a;
        public static final int buttonRight=0x7f090019;
        public static final int buttonSpace=0x7f090001;
        public static final int buttonStart=0x7f090049;
        public static final int buttonStop=0x7f09004b;
        public static final int cdToggleButton=0x7f090062;
        public static final int change_car=0x7f090010;
        public static final int change_horn=0x7f09000f;
        public static final int change_music=0x7f090009;
        public static final int change_reset=0x7f09000a;
        public static final int clearClipboard=0x7f090061;
        public static final int clipboardText=0x7f090071;
        public static final int closeopen=0x7f090089;
        public static final int cursorToggleButton=0x7f090034;
        public static final int digitalClock=0x7f090028;
        public static final int dummy_button=0x7f090041;
        public static final int editTextX=0x7f09000d;
        public static final int emptyBinButton=0x7f090063;
        public static final int fullscreen_content=0x7f09003a;
        public static final int fullscreen_content_controls=0x7f09003b;
        public static final int goButton=0x7f09006e;
        public static final int goHardButton=0x7f09006b;
        public static final int hardcontrolSpinner=0x7f09006d;
        public static final int horView=0x7f09001b;
        public static final int imageButtonBckspc=0x7f090023;
        public static final int imageButtonDown=0x7f09001e;
        public static final int imageButtonEnd=0x7f090027;
        public static final int imageButtonEnter=0x7f090022;
        public static final int imageButtonEsc=0x7f090024;
        public static final int imageButtonFast=0x7f090084;
        public static final int imageButtonHome=0x7f090026;
        public static final int imageButtonLeft=0x7f090020;
        public static final int imageButtonMouse=0x7f090015;
        public static final int imageButtonNext=0x7f090085;
        public static final int imageButtonPlay=0x7f090083;
        public static final int imageButtonPrev=0x7f090081;
        public static final int imageButtonRepeat=0x7f090086;
        public static final int imageButtonRight=0x7f09001f;
        public static final int imageButtonSlow=0x7f090082;
        public static final int imageButtonStop=0x7f090087;
        public static final int imageButtonSuffle=0x7f090088;
        public static final int imageButtonTab=0x7f090025;
        public static final int imageButtonUp=0x7f090021;
        public static final int imageSetIP=0x7f090074;
        public static final int imageSetIPIM=0x7f090073;
        public static final int imageViewNavigation=0x7f090000;
        public static final int ipinfo=0x7f090072;
        public static final int jumpButton=0x7f090031;
        public static final int leftClick=0x7f09003f;
        public static final int leftSetRow1=0x7f09000b;
        public static final int leftSetRow2=0x7f090008;
        public static final int main=0x7f09007a;
        public static final int mainLayout=0x7f090070;
        public static final int muteToggleButton=0x7f09005c;
        public static final int navRel=0x7f09001c;
        public static final int nextSlideButton=0x7f09002a;
        public static final int openCamera=0x7f09003d;
        public static final int prevSlideButton=0x7f090029;
        public static final int quitVLC=0x7f090055;
        public static final int remoteView=0x7f09003c;
        public static final int resButton=0x7f090068;
        public static final int resSpinner=0x7f090067;
        public static final int retunTextView=0x7f090079;
        public static final int rightClick=0x7f090040;
        public static final int screenViewMode=0x7f090038;
        public static final int scroll=0x7f09006f;
        public static final int scrollV=0x7f09001a;
        public static final int scrollView=0x7f090042;
        public static final int seekBarMASTER=0x7f090044;
        public static final int seekBarMaster=0x7f09007c;
        public static final int seekBarVLC=0x7f090043;
        public static final int seekBarWMP=0x7f09007e;
        public static final int serverIP=0x7f090075;
        public static final int setX=0x7f09000c;
        public static final int setY=0x7f090013;
        public static final int slideNumEditText=0x7f090030;
        public static final int speakClipboard=0x7f090060;
        public static final int spinnerJumpType=0x7f090050;
        public static final int start=0x7f090057;
        public static final int stop=0x7f090058;
        public static final int sysSpinnerDone=0x7f090065;
        public static final int systemSpinner=0x7f090064;
        public static final int tab=0x7f09007b;
        public static final int tableLayout1=0x7f090014;
        public static final int tableRow1=0x7f090016;
        public static final int tableRow10=0x7f09005f;
        public static final int tableRow2=0x7f09002b;
        public static final int tableRow3=0x7f09002e;
        public static final int tableRow4=0x7f090032;
        public static final int tableRow5=0x7f090035;
        public static final int tableRow6=0x7f090048;
        public static final int tableRow7=0x7f090066;
        public static final int tableRow8=0x7f090069;
        public static final int tableRow9=0x7f09006c;
        public static final int terminalCmd=0x7f090076;
        public static final int terminalEditText=0x7f090078;
        public static final int textView1=0x7f09002c;
        public static final int textView2=0x7f09002f;
        public static final int titleTextView=0x7f090077;
        public static final int toggleButtonMaster=0x7f09007d;
        public static final int toggleButtonWMP=0x7f09007f;
        public static final int toggleControls=0x7f090053;
        public static final int toggleFullscreen=0x7f090054;
        public static final int toggleMaster=0x7f090045;
        public static final int toggleVLC=0x7f090046;
        public static final int touchPadView=0x7f090039;
        public static final int touchframe=0x7f09003e;
        public static final int viewTaskButton=0x7f090037;
        public static final int weapons=0x7f090003;
        public static final int wep1=0x7f090004;
        public static final int wep2=0x7f090005;
        public static final int wep3=0x7f090006;
        public static final int wep4=0x7f090007;
        public static final int wheelMouse=0x7f090018;
        public static final int zoomControl=0x7f09002d;
    }
    public static final class layout {
        public static final int activity_gaming_wheel=0x7f030000;
        public static final int activity_main=0x7f030001;
        public static final int activity_navigation_panel=0x7f030002;
        public static final int activity_power_point_control=0x7f030003;
        public static final int activity_remote=0x7f030004;
        public static final int activity_snapshot=0x7f030005;
        public static final int activity_touch_pad=0x7f030006;
        public static final int activity_touch_screen=0x7f030007;
        public static final int activity_vlc=0x7f030008;
        public static final int activity_wmp=0x7f030009;
        public static final int audio_main=0x7f03000a;
        public static final int control_room=0x7f03000b;
        public static final int grid=0x7f03000c;
        public static final int ipinfo=0x7f03000d;
        public static final int splash_screen=0x7f03000e;
        public static final int terminal=0x7f03000f;
        public static final int wmp_player=0x7f030010;
    }
    public static final class menu {
        public static final int gaming_wheel=0x7f080000;
        public static final int navigation_panel=0x7f080001;
        public static final int power_point_control=0x7f080002;
        public static final int vlc=0x7f080003;
        public static final int wm=0x7f080004;
    }
    public static final class string {
        public static final int _000_000_000_000=0x7f06000b;
        public static final int _cd_rom_eject=0x7f06001b;
        public static final int _clipboard=0x7f060012;
        public static final int _monitor=0x7f060015;
        public static final int _recycle_bin_cleaner=0x7f06001c;
        public static final int accelerometer_mouse=0x7f06002f;
        public static final int action_settings=0x7f060001;
        public static final int air_mouse_control=0x7f060031;
        public static final int app_name=0x7f060000;
        public static final int automatic=0x7f060043;
        public static final int brightness_control=0x7f06000e;
        public static final int c_=0x7f060023;
        public static final int cd_rom=0x7f06001a;
        public static final int change=0x7f060018;
        public static final int change_resolution=0x7f060017;
        public static final int clear=0x7f060011;
        public static final int clear_recycle_bin=0x7f060048;
        public static final int clipboard=0x7f060049;
        public static final int clipboard_text=0x7f060006;
        public static final int digitalclock=0x7f06003e;
        public static final int done=0x7f060016;
        public static final int dummy_button=0x7f06001e;
        public static final int dummy_content=0x7f06001f;
        public static final int echo=0x7f060021;
        public static final int enter_windows_command=0x7f060020;
        public static final int exit=0x7f060026;
        public static final int go=0x7f06003b;
        public static final int go_to_slide=0x7f060041;
        public static final int go_to_touchpad=0x7f060047;
        public static final int half=0x7f060010;
        public static final int hello_world=0x7f060002;
        public static final int javacodegeeks=0x7f060003;
        public static final int jump_now=0x7f060044;
        public static final int left=0x7f060028;
        public static final int leftclick=0x7f060034;
        public static final int master_audio_control=0x7f06000d;
        public static final int master_volume=0x7f06004c;
        public static final int monitor=0x7f060014;
        public static final int mute_audio=0x7f06000f;
        public static final int mute_master_audio=0x7f060007;
        public static final int next_slide=0x7f060042;
        public static final int open_camera=0x7f060039;
        public static final int pad_less_mouse=0x7f060024;
        public static final int playback=0x7f06004e;
        public static final int prev_slide=0x7f06003f;
        public static final int quit_vlc=0x7f06004f;
        public static final int replay=0x7f060050;
        public static final int right=0x7f060029;
        public static final int rightclick=0x7f06003c;
        public static final int screen_saver=0x7f060013;
        public static final int send=0x7f060004;
        public static final int server_ip_address=0x7f06000a;
        public static final int set_desktop_background=0x7f060038;
        public static final int set_server_ip=0x7f06000c;
        public static final int shaker_demo=0x7f060027;
        public static final int shell_command=0x7f060025;
        public static final int speak=0x7f060008;
        public static final int start_pause=0x7f06004d;
        public static final int title_activity_gaming_wheel=0x7f060052;
        public static final int title_activity_navigation_panel=0x7f060054;
        public static final int title_activity_power_point_control=0x7f06003d;
        public static final int title_activity_remote=0x7f06003a;
        public static final int title_activity_snapshot=0x7f060037;
        public static final int title_activity_splash=0x7f06002d;
        public static final int title_activity_splash_s=0x7f060035;
        public static final int title_activity_splash_screen=0x7f060036;
        public static final int title_activity_terminal=0x7f06001d;
        public static final int title_activity_touch_pad=0x7f060030;
        public static final int title_activity_touch_screen=0x7f060055;
        public static final int title_activity_vlc=0x7f060051;
        public static final int title_activity_wmp=0x7f06004a;
        public static final int todo=0x7f06002e;
        public static final int togglebutton=0x7f060009;
        public static final int touch_pad=0x7f060032;
        public static final int touch_screen=0x7f060056;
        public static final int touchpad=0x7f060033;
        public static final int turn_monitor=0x7f060005;
        public static final int view_power_point_screen=0x7f060046;
        public static final int view_taskbar=0x7f060045;
        public static final int vlc_volume=0x7f06004b;
        public static final int watch_out_takerover_server_popup=0x7f060053;
        public static final int window_media_player_audio=0x7f060019;
        public static final int windows_shell=0x7f060022;
        public static final int x_axis=0x7f06002a;
        public static final int y_axis=0x7f06002b;
        public static final int z_axis=0x7f06002c;
        public static final int zoom=0x7f060040;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.

    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.

        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.

    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f070000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f070001;
        public static final int ButtonBar=0x7f070003;
        public static final int ButtonBarButton=0x7f070004;
        public static final int FullscreenActionBarStyle=0x7f070005;
        public static final int FullscreenTheme=0x7f070002;
    }
    public static final class styleable {
        /** 
         Declare custom theme attributes that allow changing which styles are
         used for button bars depending on the API level.
         ?android:attr/buttonBarStyle is new as of API 11 so this is
         necessary to support previous API levels.
    
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #ButtonBarContainerTheme_buttonBarButtonStyle com.takeover:buttonBarButtonStyle}</code></td><td></td></tr>
           <tr><td><code>{@link #ButtonBarContainerTheme_buttonBarStyle com.takeover:buttonBarStyle}</code></td><td></td></tr>
           </table>
           @see #ButtonBarContainerTheme_buttonBarButtonStyle
           @see #ButtonBarContainerTheme_buttonBarStyle
         */
        public static final int[] ButtonBarContainerTheme = {
            0x7f010000, 0x7f010001
        };
        /**
          <p>This symbol is the offset where the {@link com.takeover.R.attr#buttonBarButtonStyle}
          attribute's value can be found in the {@link #ButtonBarContainerTheme} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name com.takeover:buttonBarButtonStyle
        */
        public static final int ButtonBarContainerTheme_buttonBarButtonStyle = 1;
        /**
          <p>This symbol is the offset where the {@link com.takeover.R.attr#buttonBarStyle}
          attribute's value can be found in the {@link #ButtonBarContainerTheme} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name com.takeover:buttonBarStyle
        */
        public static final int ButtonBarContainerTheme_buttonBarStyle = 0;
    };
}
